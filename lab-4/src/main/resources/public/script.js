/**
 * Created by maciej.debowski on 2015-10-31.
 */

$(document).ready(function () {
    $.ajax({
        url: "http://localhost:4567/places", success: function (result) {
            $("#places").empty();
            $.each(result, function (index, value) {
                var place = value.name;
                $("#places").append(
                    $('<option></option>').val(place).html(place)
                );
            });
            updateTimeTable($("#places").val());
        }
    });

    $.ajax({
        url: "http://localhost:4567/lecturers", success: function (result) {
            $.each(result, function (index, value) {
                var lect = value.title + ' ' + value.firstName + ' ' + value.lastName;
                $("#dialog-lecturers").append(
                    $('<option></option>').val(lect).html(lect)
                );
            });
        }
    });

    var h = document.body.offsetHeight;
    var w = document.body.offsetWidth;
    document.getElementById('disablingDiv').style.height = h + "px";
    document.getElementById('disablingDiv').style.width = w + "px";

    var dialog = document.getElementById('window');

    $(document).on('click', ".has-events", function (event) {
        if($(event.target).parent()[0].classList.contains('delete')){
            return;
        }
        var startHour = $(event.target).closest('tr').attr('id');
        var dayOfWeek = $(event.target).closest('td').attr('headers');
        var id = $($(event.target).closest('td')).find('a.delete').attr('id');
        $("#title").html('Starts at ' + startHour + ' on ' + dayOfWeek);
        $('#dialog-classes').val($($(event.target).closest('tr')).find('span.title').text());
        $('#dialog-lecturers option[value="' + $($(event.target).closest('tr')).find('span.lecturer').text().trim() + '"]').attr('selected','selected');

        $("#endHour").empty();
        var endHour = addHalf(startHour, 1);
        var count = 0;
        while(endHour != "20:00") {
            count++;
            endHour = addHalf(endHour, 1);
            var endCell = findCell(endHour.split(':')[0], endHour.split(':')[1], dayOfWeek);
            if(endCell != undefined){
                if(endCell.classList.contains('no-events')){
                    $("#endHour").append(
                        $('<option></option>').val(endHour).html(endHour)
                    );
                } else {
                    break;
                }
            }
        }

        document.getElementById('disablingDiv').style.display = 'block';
        dialog.className = 'update';
        $('button#save').removeAttr('class').addClass(id);
        dialog.show();
    });

    $(document).on('click', ".no-events", function (event) {
        var startHour = $(event.target).parent()[0].id;
        var dayOfWeek = event.target.headers;

        //check if next is available
        var next = addHalf(startHour, 1);
        var nextHour = next.split(':')[0];
        var nextMinute = next.split(':')[1];
        if(findCell(nextHour, nextMinute, dayOfWeek).classList.contains("has-events")){
            return;
        }

        $("#title").html('Starts at ' + startHour + ' on ' + dayOfWeek);

        document.getElementById('disablingDiv').style.display = 'block';

        $("#endHour").empty();
        var endHour = addHalf(startHour, 1);
        var count = 0;
        while(endHour != "20:00") {
            count++;
            endHour = addHalf(endHour, 1);
            var endCell = findCell(endHour.split(':')[0], endHour.split(':')[1], dayOfWeek);
            if(endCell.classList.contains('no-events')){
                $("#endHour").append(
                    $('<option></option>').val(endHour).html(endHour)
                );
            } else {
                break;
            }
        }
        dialog.className = 'create';
        dialog.show();
    });

    document.getElementById('save').onclick = function () {
        var request = function(name, title, firstName, lastName, place, day, startHour, startMinute, endHour, endMinute, halfHour){return '{ \
            "name": "' + name + '", \
            "lecturer": { \
            "title": "'+title+'", \
                "firstName": "' +firstName+ '", \
                "lastName": "'+lastName+'" \
        }, \
            "place": { \
            "name": "'+place+'" \
        }, \
            "time": { \
            "dayOfWeek": "'+day+'", \
                "startTime": { \
                "hour": '+startHour+', \
                    "minute": '+startMinute+', \
                    "second": 0, \
                    "nano": 0 \
            }, \
            "finishTime": { \
                "hour": '+endHour+', \
                    "minute": '+endMinute+', \
                    "second": 0, \
                    "nano": 0 \
            }, \
            "halfHoursBetween": '+halfHour+' \
        } \
        }'; };

        var name = $("#dialog-classes").val();
        var title = $("#dialog-lecturers").val().split(' ')[0];
        var firstName = $("#dialog-lecturers").val().split(' ')[1];
        var lastName = $("#dialog-lecturers").val().split(' ')[2];
        var place = $("#places").val();
        var day = $('#title').text().split(' ')[4];
        var startHour = $('#title').text().split(' ')[2].split(':')[0];
        var startMinute = $('#title').text().split(' ')[2].split(':')[1];
        var endHour = $('#endHour').val().split(':')[0];
        var endMinute = $('#endHour').val().split(':')[1];
        var half = (parseInt(endHour) - parseInt(startHour))*2;
        half = half + (parseInt(endMinute) - parseInt(startMinute))/30;

        if($('dialog#window').hasClass('create')){
            $.post("http://localhost:4567/classes",
                request(name, title, firstName, lastName, place, day, parseInt(startHour), parseInt(startMinute), parseInt(endHour), parseInt(endMinute), half),
                function(data, status){
                    updateTimeTable(place);
                });
        } else {
            $.post("http://localhost:4567/classes/" + $('button#save').attr('class'),
                request(name, title, firstName, lastName, place, day, parseInt(startHour), parseInt(startMinute), parseInt(endHour), parseInt(endMinute), half),
                function(data, status){
                    updateTimeTable(place);
                });
        }
        document.getElementById('disablingDiv').style.display = 'none';
        dialog.close();
    };

    document.getElementById('cancel').onclick = function () {
        document.getElementById('disablingDiv').style.display = 'none';
        dialog.close();
    };

});

var addHalf = function(hour, half) {
    var real = parseInt(hour.split(':')[0]) + (hour.split(':')[1] === "00" ? 0 : .5) + half/2;
    var floor = Math.floor(real);
    return (floor < 10 ? '0' + floor : floor) + ':' + (floor == real ? "00" : "30");
};

var clearTimeTable = function () {
    $('table td:not(:first-child)').remove();
    $("tr").each(function () {
        var value = $(this).children().first().html();
        if (value != "&nbsp;") {
            var cell = $(this);
            $("th").each(function () {
                if ($(this).html() != "&nbsp;") {
                    var noEventElem = '<td headers="' + $(this).attr('id') + '" class=" no-events" rowspan="1"></td>';
                    cell.append(noEventElem);
                }
            });
        }
    });
};

var findCell = function (hour, minute, day) {
    return $('tr[id="' + hour + ':' + minute + '"]').children("td[headers='" + day + "']")[0];
};

var updateTimeTable = function (place) {
    clearTimeTable();
    var eventContent = function (title, lecturer, id) {
        return '<div class="row-fluid practice" style="width: 99%; height: 100%;"> \
                            <span class="title">' + title + '</span> \
				            <span class="lecturer"> \
						        <a>' + lecturer + '</a> \
				            </span> \
				            <a href="#" class="delete" onclick="deleteClasses(this)" id="' + id + '"><strong>x</strong></a> \
                        </div>';
    };

    $.ajax({
        url: "http://localhost:4567/classes/places/" + place, success: function (result) {
            $.each(result, function (index, value) {
                var lecturer = value.lecturer.title + " " + value.lecturer.firstName + " " + value.lecturer.lastName;
                var name = value.name;
                var day = value.time.dayOfWeek;
                var hour = (value.time.startTime.hour < 10 ? '0' + value.time.startTime.hour : value.time.startTime.hour.toString() );
                var minute = (value.time.startTime.minute == 0 ? '00' : '30');
                var id = value.id;

                //remove useless no-event divs
                for(i = 1; i < value.time.halfHoursBetween; i++){
                    var toBeRemoved = findCell(addHalf(hour+':'+minute, i).split(':')[0], addHalf(hour+':'+minute, i).split(':')[1], day);
                    toBeRemoved.remove();
                }
                //10:00 - 10:30 - 11:00

                var cell = findCell(hour, minute, day);
                $(cell).removeClass("no-events").addClass("has-events");
                $(cell).attr('rowspan', value.time.halfHoursBetween);
                $(cell).html(eventContent(name, lecturer, id));
            });
        }
    });

    $("#places").change(function () {
        updateTimeTable(this.value);

    });
};

var deleteClasses = function (a) {
    $.ajax({
        method: "DELETE",
        url: "http://localhost:4567/classes/" + a.id,
        success: function (result) {
            updateTimeTable($("#places").val());

        }
    });
};

function deselect(e) {
    $('.pop').slideFadeToggle(function () {
        e.removeClass('selected');
    });
}
;

$('#contact').on('click', function () {
    if ($(this).hasClass('selected')) {
        deselect($(this));
    } else {
        $(this).addClass('selected');
        $('.pop').slideFadeToggle();
    }
    return false;
});

$('.close').on('click', function () {
    deselect($('#contact'));
    return false;
});


