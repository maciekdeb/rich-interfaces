package pl.lodz.p.weeia;

import spark.Response;

import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

public class ClassesService {

    private Map<String, Classes> classes = new HashMap<>();

    public List<Classes> getAllClasses() {
        return new ArrayList<>(classes.values());
    }

    public Set<Place> getAllPlaces() {
        Set<Place> places = classes.values().stream().map(Classes::getPlace).collect(Collectors.toSet());
        return places;
    }

    public List<Classes> getClassesByPlace(Place place) {
        List<Classes> result = classes.values().stream().filter(c -> c.getPlace().equals(place)).collect(Collectors.toList());
        return result;
    }

    public Classes getClass(String id) {
        return classes.get(id);
    }

    public Classes createClasses(Classes classes) {
        return createClasses(classes.getName(), classes.getLecturer(), classes.getPlace(), classes.getTime());
    }

    public Classes createClasses(String name, Lecturer lecturer, Place place, Time time) {
        failIfInvalid(name, place, lecturer, time);
        Classes classes = new Classes(name, lecturer, place, time);
        this.classes.put(classes.getId(), classes);
        return classes;
    }

    public Classes deleteClasses(String id) {
        return classes.remove(id);
    }

    public Classes updateClasses(String id, Classes classes) {
        return updateClasses(id, classes.getName(), classes.getPlace(), classes.getLecturer(), classes.getTime());
    }

    public Classes updateClasses(String id, String name, Place place, Lecturer lecturer, Time time) {
        Classes classes = this.classes.get(id);
        if (classes == null) {
            throw new IllegalArgumentException("No classes with id '" + id + "' found");
        }
        failIfInvalid(name, place, lecturer, time);
        classes.setName(name);
        classes.setPlace(place);
        classes.setLecturer(lecturer);
        classes.setTime(time);
        return classes;
    }

    private void failIfInvalid(String name, Place place, Lecturer lecturer, Time time) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Parameter 'name' cannot be empty");
        }
        if (place == null || place.getName() == null || place.getName().isEmpty()) {
            throw new IllegalArgumentException("Parameter 'place' cannot be empty");
        }
        if (lecturer == null) {
            throw new IllegalArgumentException("Parameter 'lecturer' cannot be empty");
        }
        if (!validDate(time)) {
            throw new IllegalArgumentException("Parameter 'time' is wrong");
        }
    }

    public static boolean validDate(Time time) {
        final LocalTime min = LocalTime.of(8, 0);
        final LocalTime max = LocalTime.of(20, 0);

        if (time == null || time.getDayOfWeek() == null || time.getFinishTime() == null || time.getStartTime() == null) {
            return false;
        }

        LocalTime start = time.getStartTime();
        LocalTime finish = time.getFinishTime();

        if (start.isAfter(finish) || start.equals(finish)) {
            return false;
        }

        if (start.isBefore(min) || start.isAfter(max) || (start.getMinute() != 0 && start.getMinute() != 30)) {
            return false;
        }
        if (finish.isBefore(min) || finish.isAfter(max) || (finish.getMinute() != 0 && finish.getMinute() != 30)) {
            return false;
        }

        return true;
    }

    public Set<Lecturer> getAllLecturers() {
        Set<Lecturer> lecturers = classes.values().stream().map(Classes::getLecturer).collect(Collectors.toSet());
        return lecturers;
    }
}
