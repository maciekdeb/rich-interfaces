package pl.lodz.p.weeia;

import java.util.UUID;

/**
 * Created by maciek on 25/10/15.
 */
public class Lecturer {

    private String id;
    private String title;
    private String firstName;
    private String lastName;

    public Lecturer() {
    }

    public Lecturer(String firstName, String lastName, String title) {
        this.id = UUID.randomUUID().toString();
        this.firstName = firstName;
        this.lastName = lastName;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lecturer lecturer = (Lecturer) o;

        if (!title.equals(lecturer.title)) return false;
        if (!firstName.equals(lecturer.firstName)) return false;
        return lastName.equals(lecturer.lastName);

    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        return result;
    }
}
