package pl.lodz.p.weeia;

import java.util.UUID;

/**
 * Created by maciek on 25/10/15.
 */
public class Place {

    private String id;
    private String name;

    public Place() {
    }

    public Place(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Place place = (Place) o;

        return name.equals(place.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
