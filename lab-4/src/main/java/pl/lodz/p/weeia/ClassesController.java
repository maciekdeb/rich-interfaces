package pl.lodz.p.weeia;

import spark.Spark;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

import static spark.Spark.*;


public class ClassesController {

	private static final String CLASSES_URL = "/classes";
	private static final String PLACES_URL = "/places";

	public ClassesController(final ClassesService classesService) {

		Spark.get(PLACES_URL, (request, response) -> classesService.getAllPlaces(), JsonUtil.json());

		Spark.get(CLASSES_URL, (req, res) -> classesService.getAllClasses(), JsonUtil.json());

		Spark.get(CLASSES_URL + "/places" + "/:placeName", (req, res) -> {
			String placeName = req.params(":placeName");
			List<Classes> classes = classesService.getClassesByPlace(new Place(placeName));
			if (classes != null) {
				return classes;
			}
			res.status(400);
			return new ResponseError("No place with name '%s' found", placeName);
		}, JsonUtil.json());

		Spark.get("/lecturers", (req, res) -> {
			Set<Lecturer> lecturers = classesService.getAllLecturers();
			return lecturers;
		}, JsonUtil.json());

		Spark.get(CLASSES_URL + "/:id", (req, res) -> {
			String id = req.params(":id");
			Classes classes = classesService.getClass(id);
			if (classes != null) {
				return classes;
			}
			res.status(400);
			return new ResponseError("No classes with id '%s' found", id);
		}, JsonUtil.json());

		Spark.post(CLASSES_URL, (req, res) -> classesService.createClasses(JsonUtil.fromJson(req.body(), Classes.class)), JsonUtil.json());

		Spark.post(CLASSES_URL + "/:id", (req, res) -> classesService.updateClasses(req.params(":id"), JsonUtil.fromJson(req.body(), Classes.class)), JsonUtil.json());

		Spark.delete(CLASSES_URL + "/:id", (request, response) -> classesService.deleteClasses(request.params(":id")), JsonUtil.json());

		after((req, res) -> {
			res.type("application/json");
		});

		exception(IllegalArgumentException.class, (e, req, res) -> {
			res.status(400);
			res.body(JsonUtil.toJson(new ResponseError(e)));
		});

	}
}
