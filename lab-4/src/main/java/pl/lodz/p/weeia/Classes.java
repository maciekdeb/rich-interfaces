package pl.lodz.p.weeia;

import java.util.UUID;

public class Classes {

	private String id;
	private String name;
	private Lecturer lecturer;
	private Place place;
	private Time time;

    public Classes(String name, Lecturer lecturer, Place place, Time time) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.lecturer = lecturer;
        this.place = place;
        this.time = time;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public Lecturer getLecturer() {
        return lecturer;
    }

    public void setLecturer(Lecturer lecturer) {
        this.lecturer = lecturer;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }
}
