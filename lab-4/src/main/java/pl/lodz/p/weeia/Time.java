package pl.lodz.p.weeia;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;

/**
 * Created by maciek on 25/10/15.
 */
public class Time {

    private DayOfWeek dayOfWeek;
    private LocalTime startTime;
    private LocalTime finishTime;
    private long halfHoursBetween;

    public Time() {
    }

    public Time(DayOfWeek dayOfWeek, LocalTime startTime, LocalTime finishTime) {
        this.dayOfWeek = dayOfWeek;
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.halfHoursBetween = ChronoUnit.MINUTES.between(startTime, finishTime) / 30;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public long getHalfHoursBetween() {
        return halfHoursBetween;
    }

    public void setHalfHoursBetween(long halfHoursBetween) {
        this.halfHoursBetween = halfHoursBetween;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(LocalTime finishTime) {
        this.finishTime = finishTime;
    }
}
