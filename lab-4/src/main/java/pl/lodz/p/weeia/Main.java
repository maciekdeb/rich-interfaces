package pl.lodz.p.weeia;

import java.time.DayOfWeek;
import java.time.LocalTime;

import static spark.SparkBase.staticFileLocation;

public class Main {
	public static void main(String[] args) {
		staticFileLocation("/public");

        ClassesService classesService = new ClassesService();
        new ClassesController(classesService);

        Lecturer lecturer = new Lecturer("Piotr", "Kowalski", "Prof.");
        Place place = new Place("54A");
        Time time = new Time(DayOfWeek.MONDAY, LocalTime.of(8, 30), LocalTime.of(9, 30));
        Classes classes = new Classes("Matematyka", lecturer, place, time);
        classesService.createClasses(classes);

        Lecturer lecturer2 = new Lecturer("Andrzej", "Nowak", "Prof.");
        Time time2 = new Time(DayOfWeek.FRIDAY, LocalTime.of(10, 30), LocalTime.of(12, 0));
        Classes classes2 = new Classes("Fizyka", lecturer2, place, time2);
        classesService.createClasses(classes2);

        Lecturer lecturer3 = new Lecturer("Piotr", "Kowalski", "Prof.");
        Place place3 = new Place("20B");
        Time time3 = new Time(DayOfWeek.THURSDAY, LocalTime.of(10, 30), LocalTime.of(14, 30));
        Classes classes3 = new Classes("Angielski", lecturer3, place3, time3);
        classesService.createClasses(classes3);

    }
}
