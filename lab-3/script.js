
var listOfGameElements = [];
var lastGameElementClicked;
var secoundGameElementClicked;

var pairToReviled = 6;
var reviledPairs = 0;


//Stateful programming !!!!


// Game Element state - hidden , not-hidden, resolved
var gameElementState = {
		hidden : 0,
		unhidden : 1,
		resolved : 2
}
// Game state
var gameState = {
		zeroElementReviled : 0,
		oneElementReviled : 1,
		twoElementReviled : 2,
		gameEnded: 3
}
var currentGameState = gameState.zeroElementReviled;


function loadBoard(){
	
	populateListOfGameElements();
	
	var board = document.getElementById("board");
	for (var i = 0, row; row = board.rows[i]; i++) {
		for(var j = 0, cell; cell = row.cells[j]; j++){
			cell.appendChild(getAndRemoveRandomGameElement(listOfGameElements));			
		}
	}
}

function populateListOfGameElements(){
	insertGameElement("images/red.png", 2, listOfGameElements);
	insertGameElement("images/blue.png", 2, listOfGameElements);
	insertGameElement("images/green.png", 2, listOfGameElements);
	insertGameElement("images/yellow.png", 2, listOfGameElements);
	insertGameElement("images/purple.png", 2, listOfGameElements);
	insertGameElement("images/black.png", 2, listOfGameElements);
}

function insertGameElement(source, count, elementList){
	for(var i=0; i<count; i++){
		elementList.push(createGameElement("images/dot.png", source
				,clickHandler));
	}
}

function clickHandler(event){
	var gameElement = event.target;
	gameElement.turn();
	updateGameState(gameElement);
}


function updateGameState(gameElement){
	switch (currentGameState) {
	case gameState.zeroElementReviled:
		lastGameElementClicked = gameElement
		currentGameState = gameState.oneElementReviled;
		break;
	case gameState.oneElementReviled:
		if(gameElementEquals(gameElement, lastGameElementClicked)){
			gameElement.resolve();
			lastGameElementClicked.resolve();
			currentGameState = gameState.zeroElementReviled;
			reviledPairs++;
		}else{			
			currentGameState = gameState.twoElementReviled;
			secoundGameElementClicked = gameElement;
		}
		break;
	case gameState.twoElementReviled:
			secoundGameElementClicked.hideElement();
			lastGameElementClicked.hideElement();
			lastGameElementClicked = gameElement;
			currentGameState = gameState.oneElementReviled;
	case gameState.endGame:
		break;
	default:
		break;
	}
	checkGameEnd();
}

function checkGameEnd() {
	if(reviledPairs >= pairToReviled){
		currentGameState = gameState.gameEnded;
		alert("Game has ended, press F5 to reset");
	}
}

function gameElementEquals(thisGameElement,thatGameElement){
	return thisGameElement.notHiddenSrc === thatGameElement.notHiddenSrc;
}

function getAndRemoveRandomGameElement(gameElements){
	var randomNumber = getRandomArbitrary(0, gameElements.length);
	var gameElement = gameElements[randomNumber];
	gameElements.splice(randomNumber, 1);
	return gameElement;
}

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}