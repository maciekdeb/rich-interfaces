function revealElement(){
	this.src = this.notHiddenSrc;
	this.imgState = gameElementState.unhidden;
}

function hideElement(){
	this.src = this.hiddenSrc;
	this.imgState = gameElementState.hidden;
}

function turn(){
	switch(this.imgState){
	case gameElementState.hidden:
		this.revealElement();
		break;
	case gameElementState.unhidden:
		this.hideElement();
		break;
	default:
	}
}

function resolve(){
	this.revealElement();
	this.imgState = gameElementState.resolved;
}



function createGameElement(hiddenSrc, notHiddenSrc, clickEvent){
	var gameElement = document.createElement("img");
	gameElement.src = hiddenSrc;
	gameElement.addEventListener("click",clickEvent);
	gameElement.notHiddenSrc = notHiddenSrc;
	gameElement.hiddenSrc = hiddenSrc;
	gameElement.imgState = gameElementState.hidden;
	gameElement.revealElement = revealElement;
	gameElement.hideElement = hideElement;
	gameElement.turn = turn;
	gameElement.resolve = resolve;
	
	
	return gameElement;
}